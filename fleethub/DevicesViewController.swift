//
//  DevicesOnMapController.swift
//  fleethub
//
//  Created by Ammar Naqvi on 3/16/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftHTTP
import JSONJoy

class DevicesViewController: UIViewController {
    
    override func loadView() {
        
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true

        
        self.view = mapView
        
        if let mylocation = mapView.myLocation {
            print("User's location: \(mylocation)")
        } else {
            print("User's location is unknown")
        }
        
        let user = Util.sharedInstance.getUser()
        
        var devices: String = ""
        for device in user.userAssignedDevices {
            devices.append(device.deviceId)
            devices.append(",")
        }
        
        print(devices)
        let params = ["deviceIds": devices]
        do {
            let opt = try HTTP.POST("http://111.68.101.29/ivms/rest/ws/devicesLocation", parameters: params)
            opt.start { response in
                if let error = response.error {
                    print("got an error: \(error)")
                    return
                }
                print(response.text ?? "")
                do {
                    let devicesLocation = try DevicesLocation(JSONDecoder(response.text ?? ""))
                    Util.sharedInstance.setDevicesLocation(devicesLocation: devicesLocation)
                    //That's it! The object has all the appropriate properties mapped.
                    
                    DispatchQueue.main.async {
                        let devicesLocation = Util.sharedInstance.getDevicesLocation()
                        // Create a GMSCameraPosition that tells the map to display the
                        // coordinate -33.86,151.20 at zoom level 6.
                        for device in devicesLocation.devices {
                            // Creates a marker in the center of the map.
                            let marker = GMSMarker()
                            marker.position = CLLocationCoordinate2D(latitude: Double(device.latitude)!, longitude: Double(device.longitude)!)
                            marker.map = mapView
                        }
                        
                    }
                } catch let error {
                    print("Error in parsing json: \(error)")
                }
                
            }
            
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
