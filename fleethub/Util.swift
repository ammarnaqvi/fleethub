//
//  Util.swift
//  fleethub
//
//  Created by Ammar Naqvi on 2/27/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//


class Util {
    private var user: User?;
    private var devicesLocation: DevicesLocation?
    
    static let sharedInstance = Util()
    
    private init() { //This prevents others from using the default '()' initializer for this class.
        
    }
    
    func setUser(user : User) {
        self.user = user;
    }
    
    func getUser() -> User {
        return user!;
    }
    
    func setDevicesLocation(devicesLocation: DevicesLocation) {
        self.devicesLocation = devicesLocation;
    }
    
    func getDevicesLocation() -> DevicesLocation {
        return devicesLocation!;
    }
}
