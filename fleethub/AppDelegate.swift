//
//  AppDelegate.swift
//  fleethub
//
//  Created by Ammar Naqvi on 3/16/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//

import UIKit
import Material
import GoogleMaps
import GooglePlaces


extension UIStoryboard {
    class func viewController(identifier: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    

    func applicationDidFinishLaunching(_ application: UIApplication) {

        GMSServices.provideAPIKey("AIzaSyD_hu3GjnJ6homSH4hKH8y9n5Ead3UXEzM")
        GMSPlacesClient.provideAPIKey("AIzaSyD_hu3GjnJ6homSH4hKH8y9n5Ead3UXEzM")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController

        let appNavigationDrawerController = AppNavigationDrawerController(rootViewController: rootViewController, leftViewController: leftViewController)
        // Configure the window with the SideNavigationController as the root view controller
        window = UIWindow(frame: Screen.bounds)
        window!.rootViewController = appNavigationDrawerController
        window!.makeKeyAndVisible()
        return
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

